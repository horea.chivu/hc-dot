PATH=~/.composer/vendor/bin:$PATH
PATH=/usr/local/bin:$PATH

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
